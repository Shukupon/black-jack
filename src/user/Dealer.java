package user;

import java.util.Collections;
import java.util.List;

import card.Card;

public class Dealer extends User {

	public Dealer(String name){
		super(name);
	}
	public void getCard(List<Card> deck) {
		this.calculate();
		while(this.getPoint() < 17) {
			System.out.println(this.getName() + "がカードを引きます");
			Collections.shuffle(deck);
			this.getHand().add(deck.get(0));
			System.out.println("Dealerの引いたカードは" + deck.get(0).getSuit() + "の" + deck.get(0).getNumber() + "です");
			deck.remove(0);
			this.calculate();
			this.showHand();
			this.showPoint();
		}
	}

}
