package user;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import card.Card;

public class User {

	private String name;
	private List<Card> hand = new ArrayList<Card>();
	private int point = 0;

	User(String name){
		this.name = name;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public List<Card> getHand() {
		return hand;
	}
	public void setHand(List<Card> hand) {
		this.hand = hand;
	}
	public int getPoint() {
		return point;
	}
	public void setPoint(int point) {
		this.point = point;
	}

	public void set(List<Card> deck) {
		for(int i = 0; i < 2; i++) {
			Collections.shuffle(deck);
			this.getHand().add(deck.get(0));
			deck.remove(0);
		}
		this.calculate();

	}

	//ユーザーのデータをリセット
	public void reset() {
		this.hand.clear();
		this.point = 0;
	}

	//手札の合計を計算
	public void calculate() {
		int sum = 0;
		//手札のポイントの合計を単純計算
		for(Card card: this.getHand()) {
			sum += card.getPoint();
		}
		this.setPoint(sum);
		//その時点の合計点数が11点を下回る場合、Aを11点と扱うかの判別
		if(this.getPoint() < 11) {
			//手札にAがあるかの判別
			for(int i = 0; i < getHand().size(); i++) {
				if(getHand().get(i).getNumber().equals("A")) {
					//Aがあれば11点として扱う
					this.setPoint(this.getPoint() + 10);

					//手札の合計点数が10点を超えるならAを11点と扱うかの判別を止める
					if(this.getPoint() > 10) {
						break;
					}
				}
			}
		}
	}

	//手札確認
	public void showHand() {
		System.out.println(this.getName() + "の手札は");
		for(int i = 0; i < this.getHand().size(); i++) {
			System.out.println(this.getHand().get(i).getSuit() + "の" + this.getHand().get(i).getNumber());
		}
		System.out.println("です");
	}

	//バーストチェック
	public boolean burstCheck() {
		return this.getPoint() > 21;
	}

	//ポイント確認
	public void showPoint() {
		System.out.println(this.getName() + "のポイントは" + this.getPoint() + "です");
	}
}
