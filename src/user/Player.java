package user;

import java.util.Collections;
import java.util.List;

import card.Card;

public class Player extends User{
	private int win = 0;
	private int lose = 0;
	private int drow = 0;
	private int chip = 100;

	public Player(String name){
		super(name);
	}

	public int getWin() {
		return win;
	}

	public void win() {
		this.win++;
	}

	public int getLose() {
		return lose;
	}

	public void lose() {
		this.lose++;
	}

	public int getDrow() {
		return drow;
	}

	public void drow() {
		this.drow++;
	}

	public int getChip() {
		return chip;
	}

	public void setChip(int chip) {
		this.chip = chip;
	}

	public void getCard(List<Card> deck) {
		Collections.shuffle(deck);
		this.getHand().add(deck.get(0));
		deck.remove(0);
		this.calculate();
	}

	//勝敗判定
	public void checkResult(Dealer dealer, int chip) {
		if((this.getPoint() < 22 && this.getPoint() > dealer.getPoint()) || dealer.getPoint() > 21) {
			System.out.println(this.getName() + "の勝ちです");
			this.win();
			this.setChip(this.getChip() + chip * 2);
		}else if(this.getPoint() == dealer.getPoint()) {
			System.out.println("引き分けです");
			this.drow();
			this.setChip(this.getChip() + chip);
		}else {
			System.out.println(this.getName() + "の負けです");
			this.lose();
		}
	}


}
