package deck;

import java.util.ArrayList;
import java.util.List;

import card.Card;

public class Deck {

	//山札
	private List<Card> deck = new ArrayList<Card>();
	//絵柄
	private String[] suits = {"スペード", "ハート", "ダイヤ", "クラブ"};
	//数字
	private String[] numbers = {"A", "2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K"};

	public List<Card> resetDeck() {
		deck.clear();
		for(String suit: suits) {
			int i = 1;
			for(String number: numbers) {
				if(i > 10) {
					i = 10;
				}
				deck.add(new Card(suit, number, i));
				i++;
			}
		}
		return deck;
	}
}
