package card;

public class Card {

	private String suit;
	private String number;
	private int point;

	public Card(String suit, String number, int point) {
		this.suit = suit;
		this.number = number;
		this.point = point;
	}

	public String getSuit() {
		return suit;
	}
	public void setSuit(String suit) {
		this.suit = suit;
	}
	public String getNumber() {
		return number;
	}
	public void setNumber(String number) {
		this.number = number;
	}
	public int getPoint() {
		return point;
	}
	public void setPoint(int point) {
		this.point = point;
	}
}
