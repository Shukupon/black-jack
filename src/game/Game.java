package game;

import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;

import card.Card;
import deck.Deck;
import user.Dealer;
import user.Player;

public class Game {
	public static void main(String[] args) {
		Scanner s = new Scanner(System.in);
		boolean quitFlag = false;
		int bet;
		System.out.println("player名を入力してください");
		System.out.print("名前:");
		String name = s.next();
		System.out.println("ようこそ" + name);
		Player player = new Player(name);
		Dealer dealer = new Dealer("Dealer");

		//chipがある&&やめない限りブラックジャックを続ける
		while(player.getChip() > 0 && !quitFlag) {
			System.out.println("Black Jackを始めます");
			player.reset();
			dealer.reset();
			List<Card> deck = new Deck().resetDeck();

			//賭けるチップの入力
			while(true) {
				try {
					System.out.println("賭けるチップの枚数を決めてください");
					System.out.println(player.getName()+ "の所持枚数:" + player.getChip() + "枚" );
					System.out.print("賭ける枚数:");
					bet = s.nextInt();
					if(bet < 1 || bet > player.getChip()) {
						System.out.println("1以上" + player.getChip() + "以下で入力してください");
						continue;
					}
					System.out.println(bet + "枚賭けます");
					player.setChip(player.getChip() - bet);
					break;
				} catch(InputMismatchException e) {
					System.out.println("1以上" + player.getChip() + "以下の整数で入力してください");
					s.next();
				}
			}


			System.out.println("まずカードが2枚ずつ" + player.getName() + "とDealerに配られます");
			player.set(deck);
			dealer.set(deck);

			System.out.println("Dealerの1枚目のカードは" + dealer.getHand().get(0).getSuit() + "の" + dealer.getHand().get(0).getNumber() + "です");
			player.showHand();
			player.showPoint();

			//playerのターン
			System.out.println(player.getName() + "のターンです");
			System.out.println("カードを引きますか？(y/n)");
			String answer = validate(s);
			for(int i = 2;; i++) {
				if(answer.equals("y")) {
					player.getCard(deck);
					System.out.println(player.getName() + "が引いたカードは" + player.getHand().get(i).getSuit() + "の" + player.getHand().get(i).getNumber() + "です");
					player.showHand();
					player.showPoint();

					//バーストのチェック
					if(player.burstCheck()) {
						System.out.println("バーストしました");
						player.checkResult(dealer, bet);
						result(player);
						break;
					}
					System.out.println("もう1枚カードを引きますか？(y/n)");
					answer = validate(s);
					if(answer.equals("y")) {
						continue;
					}else if(answer.equals("n")) {
						break;
					}
				}else if(answer.equals("n")) {
					break;
				}
			}

			//バーストしていた場合このゲームを終了
			if(player.burstCheck()) {
				if(checkChip(player)) {
					break;
				}
				//ゲーム続行のチェック
				quitFlag = checkContinue(s);
				continue;
			}

			//ディーラーのターン
			System.out.println(dealer.getName() + "のターンです");
			System.out.println(dealer.getName() + "はポイントが17以上になるまでカードを引きます");
			dealer.showHand();
			dealer.showPoint();
			dealer.getCard(deck);
			if(dealer.burstCheck()) {
				System.out.println(dealer.getName() + "がバーストしました");
			}
			player.showPoint();
			player.checkResult(dealer, bet);
			System.out.println(player.getName()+ "の所持枚数:" + player.getChip() + "枚" );
			result(player);
			if(checkChip(player)) {
				break;
			}
			quitFlag = checkContinue(s);
		}
		s.close();
	}

	//入力が正しいかの判別
	static String validate(Scanner s) {
		String answer;
		while(true) {
			answer = s.next();
			if(answer.equals("y") || answer.equals("n")) {
				break;
			}else {
				System.out.println("yまたはnで入力してください");
			}
		}
		return answer;
	}

	//チップのチェック
	static boolean checkChip(Player player) {
		if(player.getChip() == 0) {
			System.out.println("賭けるチップが無くなりました");
			System.out.println("また挑戦してください");
			return true;
		}
		return false;
	}

	//戦績の表示
	static void result(Player player) {
		System.out.println(player.getName() + "の戦績: " + player.getWin() + "勝 " + player.getLose() + "敗 " + player.getDrow() + "分け");
	}

	//ゲームを続けるかのチェック
	static boolean checkContinue(Scanner s) {
		System.out.println("ゲームを続けますか？(y/n)");

		//入力が正しいかの判別
		String continueOrquit = validate(s);
			if(continueOrquit.equals("n")) {
				System.out.println("Black Jackを終わります");
				return true;
			}
			return false;
	}
}
